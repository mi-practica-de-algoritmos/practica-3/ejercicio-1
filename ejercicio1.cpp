#include <iostream>
#include <fstream>
using namespace std;

int main(){

    int num;
    int sumatoria=0;
    int count;
    float promedio;
    ifstream archileg;
    archileg.open("Documento1.txt");
    if(!archileg){
        cout<< "No es posible abrir el archivo de lectura" << endl;
        return 1;
    }
    cout<< "Leyendo datos"<< endl;
    while (archileg>>num){
        cout<< "Lee el número "<< num <<endl;
        ++count;
        sumatoria+= num;
    }
    cout<< "Cantidad de números ingresados "<< count <<endl;
    cout<< "La sumatoria de los numeros ingresados es: "<<sumatoria <<endl;
    promedio= count > 0 ? float(sumatoria)/count : 0;
    cout<< "El promedio de los números ingresados es: "<<promedio<<endl;
    archileg.close();

    return 0;
}